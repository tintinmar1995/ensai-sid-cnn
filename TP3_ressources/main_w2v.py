import gensim
import numpy as np
from collections import Counter

wikipedia_file = "data/europarl-v7.fr-en.fr_10M"
limit = 5

class Europarl_corpus(object):
	def __iter__(self):
		for line in open(wikipedia_file):
			yield line.lower().split()

europarl_corpus = Europarl_corpus()

corpus = list()
for a in europarl_corpus.__iter__():
	corpus.append(a)

model = gensim.models.Word2Vec(europarl_corpus, iter=1, min_count=10, size=100, workers=1)
model.save("fren_w2v_model")

cnt = Counter(corpus[0])

## Question 2
print("Nombre de mots dans le corpus :      "+str(len(corpus[0])))
print("Nombre de mots dans le dictionnaire: "+str(len(cnt.values())))
print("Cinq mots les plus communs:         	"+str(cnt.most_common(5)))
print("Cinq mots les plus rares: 			"+str(cnt.most_common()[:-6:-1]))
print("Représentation vectoriel d'un mot: 	"+str(model.wv['manger']))
print("Proximité entre les vecteurs de deux mots similaires: "+str(model.wv.similarity("choisir","la")))
print("Proximité entre les vecteurs de deux mots differents: "+str(model.wv.similarity("choisir","la")))
print("Mots les plus similaires d'un mot: 	"+str(model.wv.most_similar(positive=["choisir"])))

# On a pas de base de validation. Le NN n'apprend pas à généraliser. Il se contente d'apprendre par coeur le corpus

## Question 3

model.load("fren_w2v_model")