from __future__ import print_function
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Activation
from keras.optimizers import RMSprop
import numpy as np
import random
import sys
import io


## Function to sample an index from a probability array.
#
def sample(preds, temperature=1.0):
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    # Permet de ne pas tout le tmps prendre la lettre la plus probable.
    # Plus on augmente la température, plus on se permet de choisir une lettre moins probable.
    # Cela permet d'ajouter de la diversité
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


## Function invoked at end of each epoch. Prints generated text.
def on_epoch_end(epoch, _):
    nb_occ = 3
    print()
    print('----- Generating text after Epoch: %d' % epoch)
    start_index = []
    for i in range(nb_occ):
        start_index.append(random.randint(0, len(text) - maxlen - 1))
    for diversity in [0.1, 0.5, 1]:
        print('\n---- diversity:', diversity)
        for j in range(nb_occ):
            generated = ''
            sentence = text[start_index[j]: start_index[j] + maxlen]
            generated += sentence
            print('-- Generating with seed: "' + sentence + '"')
            sys.stdout.write(generated)
            for i in range(2*maxlen):
                x_pred = np.zeros((1, maxlen, len(chars)))
                for t, char in enumerate(sentence):
                    x_pred[0, t, char_indices[char]] = 1.
                preds = model.predict(x_pred, verbose=0)[0]
                next_index = sample(preds, diversity)
                next_char = indices_char[next_index]
                generated += next_char
                sentence = sentence[1:] + next_char
                sys.stdout.write(next_char)
                sys.stdout.flush()
            print()

# open the datafile and building a dictionary of the different chars composing it
data_path = "data/europarl-v7.fr-en.fr_30k"
with io.open(data_path, encoding='utf-8') as f:
    text = f.read()
print('corpus length:', len(text))
chars = sorted(list(set(text)))
# Liste tous les caractères contenus dans le texte. On va dire que chaque lettre est une catégorie.
print('total chars:', len(chars))
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

# cut the text in semi-redundant sequences of maxlen characters
maxlen = 50
step = 1
sentences = []
next_chars = []
for i in range(0, len(text) - maxlen, step):
    sentences.append(text[i: i + maxlen])
    next_chars.append(text[i + maxlen])
print('nb sequences:', len(sentences))

print('One hot encoding of the features and label...')
x = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        x[i, t, char_indices[char]] = 1
    y[i, char_indices[next_chars[i]]] = 1


# defining the callback calling "on_epoch_end" and fitting the network
print_callback = LambdaCallback(on_epoch_end=on_epoch_end)

# chars : liste des caractères du corpus
# char_indices : dictionnaire associant pour chaque caractère l'indice associé
# indices_char : dictionnaire associant pour chaque lettre l'indice associé

# build the model
print('Build model...')
model = Sequential()

# Couche 1 : LSTM
# Input_shape est un couple : 1ère valeur le nombre de lettres regarder à chaque fois,
# 2nd valeur le nombre de mots dans le vocabulaire du corpus
model.add(LSTM(units=128, input_shape=(maxlen,len(chars))))
# model.add(LSTM(units=128, input_shape=(maxlen,len(chars)),unroll=True))

# Couche 2 : Dense, units=output_dim => softmax
model.add(Dense(units=len(chars), activation='softmax'))
# Cette couche permet de faire en sorte que la sortie soit un vecteur de somme 1
# qu'on assimile à des probas pour chaque label

# Oracle qui compare verité et prédiction (en régression c'est la MSE)
# On est dans un problème de classification (une lettre = une catégorie)
# donc on utilise categorial_crossentropy comme coût
model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(lr=0.005),
              metrics=['accuracy'])

# Le modèle fonctionne mal avec un learning rate (lr) de 0.05
# Si on le met trop bas, cela prend plus de temps et on prend le risque de rester coincer dans des
# minimas locaux

model.summary()
model.fit(x = x, y = y, batch_size= 128, epochs= 50, verbose= 2, callbacks= [print_callback])

# On place dans fit le callbacks qui est un moyen de visaulité l'apprentissage.
# Ici on a surclassé la fonction onEpochEnd, ce qui implique que le programme affiche des infos après chaque epoch


# Comment optimiser la valeur de maxlen ?

# En pratique, on utilise surtout la prédiction mot à mot et non lettre par lettre...