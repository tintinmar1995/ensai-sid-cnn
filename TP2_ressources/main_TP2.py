import numpy as np
import keras
import h5py
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Conv2D, MaxPooling2D, Flatten, Dropout, Input
from keras.optimizers import rmsprop
from keras.callbacks import EarlyStopping
import keras.backend as K
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

d_cnn = 0.2
d_fc = 0.5

# Qu'est ce qu'un epochs ? nbr d'itérations, on fait passer plusieurs fois les images
epochs = 10
batch_size = 32


def build_cnn(input_dim, output_dim, lr=0.00005):
    global d_cnn
    global d_fc


    model = Sequential()  # On lui prévient qu'on va empiler les couches (couches séquentielles) --> Sequential est une api

    # Couche 1 : Conv2D, kernel 3x3, depth=30, input_shape=input_dim => relu => MaxPooling2D, size=2x2
    model.add(keras.layers.Conv2D(30, (3, 3), input_shape=input_dim, activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    model.add(Dropout(
        rate=d_cnn))  # Grâce au dropout, on flingue des neuronnes aléatoirement. Ca permet d'éviter l'overfitting. On rend le NN plus robuste

    # Couche 2 : Conv2D, kernel 3x3,depth=60 => relu => MaxPooling2D, size=2x2
    model.add(keras.layers.Conv2D(60, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    model.add(Dropout(rate=d_cnn))

    # Couche 3 : Conv2D, kernel 3x3, depth=120 =>relu => MaxPooling2D, size=2x2
    model.add(keras.layers.Conv2D(120, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    model.add(Dropout(rate=d_cnn))

    # Couche 4 : Flatten => Dense, units=150 => relu
    model.add(Flatten())
    model.add(Dense(units=150, activation='relu'))  # Ici on aggrège toutes les filtres créées précédemment
    model.add(Dropout(rate=d_fc))

    # Couche 5: Dense, units=output_dim => softmax
    model.add(Dense(units=output_dim,
                    activation='softmax'))  # Cette couche permet de faire en sorte que la sortie soit un vecteur de somme 1 qu'on assimile à des probas pour chaque label

    # Oracle qui compare verité et prédiction (en régression c'est la MSE)
    model.compile(loss='categorical_crossentropy',
                  optimizer=rmsprop(lr=lr),
                  metrics=['accuracy'])

    return model


# Q2: Construire des ImageDataGenerator avec le rescale.

train_generator = ImageDataGenerator(rescale=1.0 / 255)
valid_generator = ImageDataGenerator(rescale=1.0 / 255)

train_flow = train_generator.flow_from_directory(directory="data/train", target_size=(32, 32), class_mode='categorical')
valid_flow = train_generator.flow_from_directory(directory="data/valid", target_size=(32, 32), class_mode='categorical')


def build_train_run_save(train_flow_f, epochs_f, valid_flow_f, batch_size_f,tb_ball_back_f):
    cnn_model = build_cnn((32, 32, 3), 2)
    cnn_model.summary()
    cnn_model.fit_generator(train_flow_f,
                            epochs=epochs_f,
                            validation_data=valid_flow_f,
                            steps_per_epoch=10000 // batch_size_f,
                            validation_steps=2000 // batch_size_f,
                            callbacks=[tb_ball_back_f])

    cnn_model.evaluate_generator(valid_flow, steps=2000 // batch_size)
    cnn_model.save_weights('my_first_model_weigth.h5')  # sauvegarde des poids du modèle que l'on a fait tourner


def load_train_run_save(tb_ball_back_f, lr=0.00005):
    # on recrée exactement le même modèle pour être sur d'avoir la même forme
    # cette fois il y a 10 classes en sortie
    # La fin du réseau de neuronnes sera surement amenée à changer pour avoir une dimension adéquate
    cnn_model_pretrained = build_cnn((32, 32, 3), 10)
    cnn_model_pretrained.load_weights('pretrained_model.hdf5')

    # suppression des deux dernières couches du modèle et ajout d'une nouvelle couche
    cnn_model_pretrained.pop()
    cnn_model_pretrained.pop()

    # ce qui est passé entre les deuxièmes parenthèses indique à la couche à quoi elle est connectée.
    # C'est comme ça que fonctionne l'API "Model()",
    # qui peut être utilisée à la place de l'API "Sequential()" que voustbCallBack connaissez déjà.
    out = Dense(2)(cnn_model_pretrained.layers[-1].output)
    # out =

    # https://stackoverflow.com/questions/44747343/keras-input-explanation-input-shape-units-batch-size-dim-etc
    # input_cnn = Input(cnn_model_pretrained.layers[-1].input_shape[1:])
    input_cnn = Input((3,))
    cnn_model_pretrained = Model(inputs=input_cnn,
                                 outputs=out)

    cnn_model_pretrained.compile(loss='categorical_crossentropy',
                                 optimizer=rmsprop(lr=lr),
                                 metrics=['accuracy'])

    cnn_model_pretrained.summary()

    cnn_model_pretrained.fit_generator(train_flow,
                                       epochs=epochs,
                                       validation_data=valid_flow,
                                       steps_per_epoch=10000 // batch_size,
                                       validation_steps=2000 // batch_size,
                                       callbacks=tb_ball_back_f)

    cnn_model_pretrained.evaluate_generator(valid_flow, steps=2000 // batch_size)


# ##########################################################################################
#                     PREMIERE PARTIE DU TP (avec Sequential)
# ##########################################################################################

# tensorboard --logdir=./logs
tbCallBack = keras.callbacks.TensorBoard(log_dir='./logs', histogram_freq=0,
                                         write_graph=True, write_images=True)

build_train_run_save(train_flow, epochs, valid_flow, batch_size, tbCallBack)

# ##########################################################################################
#                     DEUXIEME PARTIE DU TP (avec MODEL)
# ##########################################################################################

# load_train_run_save(tbCallBack)
