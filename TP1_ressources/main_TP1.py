import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.optimizers import Adam, rmsprop, sgd
from keras.callbacks import EarlyStopping
import keras.backend as K
from sklearn.metrics import cohen_kappa_score, confusion_matrix
from TP1_ressources.fonctions_TP1 import *

   

database = np.genfromtxt("/home/Martin/Téléchargements/Réseau de neuronnes/TP1_ressources/dataset/wine_database.csv", delimiter=",", dtype=float)

#Q1: one hot encode the class and normalize the features of the database

np.random.shuffle(database)

nb_fold = 5
layer_list = [50,50,20]

gt_and_pred = np.empty(shape=(2,0)) 

for fold in range(nb_fold):
    K.clear_session()
    model = build_NN(layer_list, database.shape[1]-3, 3, lr=0.1)
    model.summary()
    training_fold, valid_fold = get_kfold_cv(database, nb_fold, fold)
    callback = EarlyStopping(patience=5)
    model.fit(training_fold[:,3:], training_fold[:,:3], batch_size=16, epochs=200, validation_data=(valid_fold[:,3:], valid_fold[:,:3]), shuffle=True, callbacks=[callback])
    fold_prediction = model.predict(valid_fold[:,3:])
    gt_and_pred = np.column_stack((gt_and_pred, np.array([np.argmax(valid_fold[:,:3], axis=1), np.argmax(fold_prediction, axis=1)])))
kappa = cohen_kappa_score(gt_and_pred[0], gt_and_pred[1], labels=[0,1,2])
conf_mat = confusion_matrix(gt_and_pred[0], gt_and_pred[1], labels=[0,1,2])
print("kappa=", kappa)
print(conf_mat)