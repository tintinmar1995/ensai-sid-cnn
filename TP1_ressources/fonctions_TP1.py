import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.optimizers import Adam, rmsprop, sgd
from keras.callbacks import EarlyStopping
import keras.backend as K
from sklearn.metrics import cohen_kappa_score, confusion_matrix
from fonctions_TP1 import *




## One hot encode the columns of the database specified in list_id.
#
#  return a copy of the database, on hot encoded. 
def one_hot_encode_database(database, list_id):
    encoded_database = np.empty(shape=(database.shape[0],0), dtype=float)
    for id in range(database.shape[1]):
        if id in list_id:
            original_column = database[:, id]
            encoded_column = keras.utils.to_categorical(original_column)
            encoded_database = np.column_stack((encoded_database,encoded_column))
        else:
             original_column = database[:, id]
             encoded_database = np.column_stack((encoded_database,encoded_column))
    return encoded_database
    

## Normalize between 0 and 1 each column of the database specified in list_id.
#
#  return a copy of the database, normalized. 
def normalize_database(database, list_id):
    encoded_database = database.copy()
    for id in list_id :
        encoded_database[:, id] = (encoded_database[:, id] + min(encoded_database[:, id]))/max(encoded_database[:, id])
    return encoded_database


## Split the input data into nb_fold equal folds. Select current_fold as the test_dataset and the stack the other folds to be the train_dataset.
# 
#  return a training_dataset and a test_dataset.
def get_kfold_cv(input_data, nb_fold, current_fold):
    folds = np.array_split(input_data,nb_fold)
    test_dataset = folds[current_fold]
    training_dataset =  np.empty(shape=(0,input_data.shape[0]), dtype=float)
    for i,a in enumerate(folds):
        if i != current_fold:
            training_dataset = np.row_stack((training_dataset,a))
    return training_dataset, test_dataset


## Construct a layer composed of dense layers, which dimensions are definded in the layer_list argument.
# 
#  return the constructed and compiled model.
def build_NN(layer_list, input_dim, output_dim, lr=0.001):
    model = Sequential()
    #Add layers
    model.add(Dense(units=64, activation='relu', input_dim=100))
    model.add(Dense(units=10, activation='softmax'))
	
    # Compile the network
    model.compile(loss='categorical_crossentropy',
              optimizer='sgd',
              metrics=['accuracy'])
    return model
