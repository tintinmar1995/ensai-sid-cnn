#!/usr/bin/python
# -*- coding: latin-1 -*-

import numpy as np
from sklearn.metrics import cohen_kappa_score, confusion_matrix
from keras.callbacks import Callback
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
from TP4_ressources.cnn import *
from TP4_ressources.tools import *


class MetricsKappa(Callback):

    def on_train_begin(self, logs={}):
        self.val_kappas = []

    '''
    def on_epoch_end(self, epoch, logs={}):
        val_predict = (np.asarray(self.model.predict(self.model.validation_data[0]))).round()
        val_targ = self.model.validation_data[1]
        _val_f1 = f1_score(val_targ, val_predict)
        _val_recall = recall_score(val_targ, val_predict)
        _val_precision = precision_score(val_targ, val_predict)
        self.val_f1s.append(_val_f1)
        self.val_recalls.append(_val_recall)
        self.val_precisions.append(_val_precision)
        print “ — val_f1: % f — val_precision: % f — val_recall % f” % (_val_f1, _val_precision, _val_recall)
        return
    '''


    def on_epoch_end(self, epoch, logs={}):
        predictions_tab = []

        valid_set = list()
        valid_set_truth = list()
        for dossier, sous_dossiers, fichiers in os.walk('data/valid'):
            for fichier in fichiers:
                if "npy" in str(os.path.join(dossier, fichier)):
                    valid_set.append(os.path.join(dossier, fichier))
                    valid_set_truth.append(truth(os.path.join(dossier, fichier)))

        for filename in valid_set:
            ndimage = preprocessing(filename, False)
            prediction = self.model.predict(ndimage, verbose=0)
            predictions_tab.append(np.argmax(prediction))

        kappa = cohen_kappa_score(valid_set_truth, predictions_tab, labels=[0, 1, 2])
        conf_mat = confusion_matrix(valid_set_truth, predictions_tab, labels=[0, 1, 2])

        self.val_kappas.append(kappa)
        print()
        print("Kappa")
        print(kappa)
        print()
        print("Matrice de confusion :")
        print(conf_mat)
        print()
        print("_____________________________________")
