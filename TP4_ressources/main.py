#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys
from TP4_ressources.cnn import *
from TP4_ressources.tools import *
from sklearn.metrics import cohen_kappa_score, confusion_matrix
import keras
import os
from keras.callbacks import TensorBoard, LambdaCallback, ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Activation, Conv2D, MaxPooling2D, Flatten, Dropout, Input, LSTM
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.optimizers import RMSprop
import numpy as np
import scipy
import random
import matplotlib.pyplot as plt
import pandas as pd
import pickle as pickle


# 6 : gun-shot
# 7 : jackhammer
# 8 : siren

# 1 colonne = 1 fenêtre : les valeurs des fréquences pour la fenêtre
# 1 ligne = 1 fréquence donnée


def predict_model(filelist, mode="default", truth_tab=None, path_poids="poids/cnn_egg_dfc0.8_Epch10_SPE14_VS5_Acc0.97.h5"):
    # ATTENTION: cette fonction est là pour vous donner un exemple de fonction predict. Vous pouvez/devez modifier
    # le fonctionnement de celle-ci pour votre propre cas. N'oubliez pas d'y inclure l'éventuel prétraitement que vous
    # faites sur vos données avant de le passer au réseau de neurones.
    # ===>TESTEZ LA VOUS MEME AVANT<===

    model = build_cnn_eeg((129, 186, 1), 3, 0.8)
    model.load_weights(path_poids)
    model.compile(loss='categorical_crossentropy',
                  optimizer=RMSprop(lr=0.00005),
                  metrics=['accuracy'])

    predictions_tab = []

    for filename in filelist:
        ndimage = preprocessing(filename)
        prediction = model.predict(ndimage)
        predictions_tab.append(np.argmax(prediction))

    if mode == "default":
        return predictions_tab

    elif mode == "confmat":
        kappa = cohen_kappa_score(truth_tab, predictions_tab, labels=[0, 1, 2])
        conf_mat = confusion_matrix(truth_tab, predictions_tab, labels=[0, 1, 2])
        return kappa, conf_mat


def main():
    essai = 1
    for i in range(essai):

        try:
            with open('sauv_stat.pickle', 'rb') as file:
                df = pickle.load(file)
            with open('list_conf.pickle', 'rb') as file:
                conf = pickle.load(file)
        except:
            df = pd.DataFrame(columns=['proba_be_in_train_0', 'proba_be_in_train_1', 'proba_be_in_train_2', 'batch_size', 'Dropout Freq', 'nb_epoch', 'Kappa'])
            conf = list()

        line = list(np.zeros(7))

        # Réinitialisation de la BDD et comptage de la BDD
        reinitiate_dataset()

        print("début")
        count_data = count_data_set()
        print(count_data)

        # Managment de la BDD (tri entre la partie train et la partie valid)
        # proba = 1-equilibrer_datatrain()
        proba = [0.2, 0.4, 0.4]

        # Calcul de poids pour rééquilibrer la BDD
        data_augmentation(count=count_data)
        print("augmenté")
        count_data = count_data_set()
        print(count_data)
        # proba = [random.randint(10, 50) * 0.001, random.randint(10, 50) * 0.001, random.randint(10, 50) * 0.001]
        line[0] = proba[0]
        line[1] = proba[1]
        line[2] = proba[2]
        create_valid_set(proba=proba)

        # Récupèration d'informations sur la data_set
        print("valid")
        count_data = count_data_set()
        print(count_data)
        length_train = sum(count_data[:2])
        length_valid = count_data[3]
        class_weight = create_class_weigth(count_data)
        temoin = get_shape()

        '''
        # Paramétrage du modèle
        d_fc = random.randint(75, 99) * 0.001  # 0.9
        epochs = random.randint(5, 15)
        batch_size = random.randint(20, length_valid)
        s_p_e = length_train // batch_size
        valid_step = length_valid // batch_size
        '''

        '''
        d_fc = 0.90
        epochs = 28
        batch_size = 32
        s_p_e = length_train // batch_size
        valid_step = length_valid // batch_size

        line[3] = batch_size
        line[4] = d_fc
        line[5] = epochs

        print(count_data)

        try:
            # Entrainement du modèle
            train(temoin, class_weight, epochs, valid_step, s_p_e, d_fc, length_valid, batch_size)

            # Validation du modèle
            valid_set = list()
            valid_set_truth = list()
            for dossier, sous_dossiers, fichiers in os.walk('data/valid'):
                for fichier in fichiers:
                    if "npy" in str(os.path.join(dossier, fichier)):
                        valid_set.append(os.path.join(dossier, fichier))
                        valid_set_truth.append(truth(os.path.join(dossier, fichier)))

            result = predict_model(valid_set,
                                   "confmat",
                                   valid_set_truth,
                                   'poids/cnn_egg_dfc' + str(d_fc) + '_Epch' + str(epochs) + '_SPE' + str(s_p_e) + '_VS' + str(
                                       valid_step) + '.h5')
            line[6] = result[0]
            df.loc[df.shape[0]] = line
            conf.append(result[1])

        except:
            print("Error")
            line[4] = "Error"
            conf.append("Error")
            print(count_data)

        with open('sauv_stat.pickle', 'wb') as handle:
            pickle.dump(df, handle)

        with open('list_conf.pickle', 'wb') as handle:
            pickle.dump(conf, handle)
        '''


def main_transfer_learning(train10, train10_3):
    # La constitunion de la dataset a été fait au préalable "à la main"

    if train10:

        '''
        for dossier, sous_dossiers, fichiers in os.walk('data10'):
            for fichier in fichiers:
                if "npy" in str(os.path.join(dossier, fichier)):
                    preprocessing(os.path.join(dossier, fichier))
        '''

        # Récupèration d'informations sur la data_set
        count_data = count_data_set('data10', 10)
        length_train = sum(count_data[:2])
        length_valid = count_data[3]
        class_weight = create_class_weigth(count_data)
        temoin = get_shape()

        d_fc = 0.8
        epochs = 15
        batch_size = 28
        s_p_e = length_train // batch_size
        valid_step = length_valid // batch_size

        print(count_data)
        data_augmentation(count_data, 10)

        # Entrainement du modèle sur les 10 classes
        # Les poids sont ensuites sauvegardés dans un fichier h5
        path_poids = train(temoin, class_weight, epochs, valid_step, s_p_e, d_fc, length_valid, batch_size, 10)

    if train10_3:

        for dossier, sous_dossiers, fichiers in os.walk('data10_3'):
            for fichier in fichiers:
                if "npy" in str(os.path.join(dossier, fichier)):
                    preprocessing(os.path.join(dossier, fichier))

        # Récupèration d'informations sur la data_set
        count_data = count_data_set('data10_3')
        length_train = sum(count_data[:2])
        length_valid = count_data[3]
        class_weight = create_class_weigth(count_data)
        temoin = get_shape()

        d_fc = 0.8
        epochs = 10
        batch_size = 32
        s_p_e = length_train // batch_size
        valid_step = length_valid // batch_size

        print(count_data)

        # Entrainement du modèle sur les 10 classes
        # Les poids sont ensuites sauvegardés dans un fichier h5
        path_poids = train_transfert_learning(temoin,
                                              class_weight,
                                              epochs,
                                              valid_step,
                                              s_p_e,
                                              d_fc,
                                              length_valid,
                                              batch_size,
                                              10)

        # Validation du modèle
        valid_set = list()
        valid_set_truth = list()
        for dossier, sous_dossiers, fichiers in os.walk('data10_3/valid'):
            for fichier in fichiers:
                if "npy" in str(os.path.join(dossier, fichier)):
                    valid_set.append(os.path.join(dossier, fichier))
                    valid_set_truth.append(truth(os.path.join(dossier, fichier)))

        result = predict_model(valid_set,
                               "confmat",
                               valid_set_truth,
                               path_poids)

        print(result[0])
        print(result[1])


def analyser_test():

    with open('sauv_stat.pickle', 'rb') as file:
        df = pickle.load(file)

    with open('list_conf.pickle', 'rb') as file:
        conf = pickle.load(file)

    df.iloc[:, 3:].columns = ['batch_size', 'Dropout_Freq', 'nb_epoch', 'Kappa']
    print(df.iloc[:, 3:].sort_values(by=['Kappa']))

    print(df.shape)

    if False:
        plt.boxplot(df.Kappa)
        plt.title("Répartition du Kappa")
        plt.xlabel("")
        plt.ylabel("Valeur du Kappa de Cohen")
        plt.show()

    if False:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.matshow(df.iloc[:, 3:].corr())
        fig.colorbar(cax)
        ax.set_yticklabels([' '] + list(df.iloc[:, 3:].columns))
        ax.set_xticklabels([' '] + list(df.iloc[:, 3:].columns))
        plt.xticks(rotation='vertical')
        plt.show()

    if False:
        plt.scatter(df.Dropout_Freq, df.Kappa)
        plt.show()

    import statsmodels.api as sm

    x = df.iloc[:,0:-2]
    x = sm.add_constant(x)
    y = df.Kappa

    # Note the difference in argument order
    model = sm.OLS(y, x).fit()
    #predictions = model.predict(X)  # make the predictions by the model

    # Print out the statistics
    print(model.summary())



main()
#main_transfer_learning(False, True)
#analyser_test()
