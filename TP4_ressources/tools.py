#!/usr/bin/python
# -*- coding: latin-1 -*-

import os

from TP4_ressources.cnn import *


def deplacer_fichier(fichier, dossier_in, dossier_out):
    os.system("mv "+dossier_in+"/"+fichier+" "+dossier_out)


# Fonction qui retourne 6, 7, 8 en prenant en argument le nom du fichier
def truth(path):
    if "/6" in path:
        return 0
    if "/7" in path:
        return 1
    if "/8" in path:
        return 2


# Fonction effectuant le preprocessing pour chaque image. Elle renvoie un tableau dans la bonne dimension.
# Le tableau renvoyé peut directement être lu par le NN et une prédiction peut être lancée.
def preprocessing(path, sysout=False):
    if sysout:
        print("Convertion tableau " + str(path) + " en image.")
    scipy.misc.imsave(path.replace("npy", "png"), np.load(path))
    ndimage = scipy.misc.imread(path.replace("npy", "png"))
    ndimage = np.expand_dims(ndimage, axis=0)
    ndimage = np.expand_dims(ndimage, axis=3)
    return ndimage


def delete_fichier(dossier, fichier):
    os.system("rm " + dossier + "/" + fichier)


def reinitiate_dataset():
    # Fusion des dossiers valid et train
    for dossier, sous_dossiers, fichiers in os.walk('data/valid/6'):
        for fichier in fichiers:
            deplacer_fichier(fichier, "data/valid/6", "data/train/6")

    for dossier, sous_dossiers, fichiers in os.walk('data/valid/7'):
        for fichier in fichiers:
            deplacer_fichier(fichier, "data/valid/7", "data/train/7")

    for dossier, sous_dossiers, fichiers in os.walk('data/valid/8'):
        for fichier in fichiers:
            deplacer_fichier(fichier, "data/valid/8", "data/train/8")

    for dossier, sous_dossiers, fichiers in os.walk('data/train'):
        for fichier in fichiers:
            if "png" in fichier:
                delete_fichier(dossier, fichier)
            elif "augment" in fichier:
                delete_fichier(dossier, fichier)
    print("Les BDD train et valid ont été fusionnées")


def equilibrer_datatrain():
    count_data = count_data_set()
    class_weight = create_class_weigth(count_data[:3])
    proba = np.array(list(class_weight.values()))
    proba = max(proba) - proba
    proba += 0.8
    proba /= max(proba)
    proba -= min(proba)/2
    return proba


def create_valid_set(dataset="data", proba=[0.6, 0.6, 0.6]):
    # Constitution de la BDD valid
    for dossier, sous_dossiers, fichiers in os.walk(dataset+'/train/6'):
        for fichier in fichiers:
            if random.random() < proba[0]:
                deplacer_fichier(fichier, dataset+"/train/6", dataset+"/valid/6")

    for dossier, sous_dossiers, fichiers in os.walk(dataset+'/train/7'):
        for fichier in fichiers:
            if random.random() < proba[1]:
                deplacer_fichier(fichier, dataset+"/train/7", dataset+"/valid/7")

    for dossier, sous_dossiers, fichiers in os.walk('data/train/8'):
        for fichier in fichiers:
            if random.random() < proba[2]:
                deplacer_fichier(fichier, dataset+"/train/8", dataset+"/valid/8")

    for dossier, sous_dossiers, fichiers in os.walk(dataset):
        for fichier in fichiers:
            if "npy" in str(os.path.join(dossier, fichier)):
                preprocessing(os.path.join(dossier, fichier))

    print("La BDD valid a été constituée")


# Renvoie un tableau contenant le nombre d'images dans les dossiers
# train 6, train 7, train 8, valid
def count_data_set(dataset="data", nb_classes=3):

    length = np.zeros(nb_classes+1)

    if nb_classes == 3:
        mini = 6
    else:
        mini = 0

    for dossier, sous_dossiers, fichiers in os.walk(dataset+'/train'):
        for fichier in fichiers:
            if ".npy" in fichier:
                for i in range(nb_classes):
                    if "/"+str(i+mini) in str(dossier):
                        length[i] = length[i] + 1

    # Constitution de la BDD valid
    for dossier, sous_dossiers, fichiers in os.walk(dataset+'/valid'):
        for fichier in fichiers:
            if "npy" in str(os.path.join(dossier, fichier)):
                length[-1] = length[-1] + 1

    return length


def create_class_weigth(count_data):
    class_weight = dict()
    for i in range(len(count_data)):
        class_weight[i] = count_data[i] / min(count_data)

    return class_weight


def get_shape():
    for dossier, sous_dossiers, fichiers in os.walk('data'):
        for fichier in fichiers:
            if ".npy" in fichier:
                return np.load(os.path.join(dossier, fichier)).shape
