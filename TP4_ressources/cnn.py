#!/usr/bin/python
# -*- coding: latin-1 -*-

import keras
import os
from keras.callbacks import TensorBoard, LambdaCallback, ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Activation, Conv2D, MaxPooling2D, Flatten, Dropout, Input, LSTM
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.optimizers import RMSprop
import numpy as np
import scipy
import random
from TP4_ressources.tools import *
from TP4_ressources.metrics_kappa import *


def data_augment_aux(tab):
    # Dans les tableaux initiaux, nous avons
    # en abscisses le temps, et en ordonnées les fréquences

    # Random swapping left / right channel : décalage temporel du signal
    temp = [list(a) for a in list(tab.transpose())]
    for i in range(random.randint(-tab.shape[1]//4, tab.shape[1]//4)):
        temp.insert(0, temp.pop())
    tab = np.array(temp).transpose()

    # Etirement des sons : on dédouble des fénêtres et on en supprime d'autre

    # Random scaling with uniform amplitudes from [0.25, 1.25]
    tab = tab * random.randrange(25, 125)*0.01

    return tab


def data_augmentation(count, nb_classe=3, nombre=None):

    count = [int(a) for a in count]
    classes = range(nb_classe)

    if nombre is None:
        nombre = [min(max(count) - count[i], count[i]) for i in classes]

    sample = [sorted(random.sample(range(count[i]), nombre[i])) for i in classes]
    pointeur = list(np.zeros(nb_classe))

    for i in classes:
        for dossier, sous_dossiers, fichiers in os.walk('data/train/'+str(i)):
            for fichier in fichiers:
                if "npy" in str(os.path.join(dossier, fichier)):
                    pointeur[i] += 1
                    if pointeur[i] in sample[i]:
                        tab = np.load(str(os.path.join(dossier, fichier)))
                        tab = data_augment_aux(tab)
                        np.save(str(os.path.join(dossier, fichier)).replace(".npy", "_augment.npy"), tab)


# Une solution trouvée fonctionnant d'après un papier pour des électroencéphalogramme
# Importation des données en STFT
# https://pos.sissa.it/299/001/pdf
# Ce réseau peut être utilisé sur les 10 ou les 3 classes
def build_cnn_eeg(input_dim, output_dim, d_fc, lr=0.00005):
    print('Importation des paramètres...')
    print('Build model...')
    model = Sequential()
    # Couche 1 : CNN + Maxpool
    model.add(keras.layers.Conv2D(15, (3, 3), input_shape=input_dim, activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    # Couche 2 : CNN + Maxpool
    model.add(keras.layers.Conv2D(40, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    # Couche 3 : CNN + Maxpool
    model.add(keras.layers.Conv2D(400, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    # Couche 4 : Dense = fullconnection, units = Nombre de classes
    model.add(Dense(units=200, activation='linear'))
    # Couche 6 : Dropout (désactive des sorties de neurones aléatoirement avec une probabilité prédéfinie)
    model.add(Dropout(rate=d_fc))
    # Couche 5 : Dense, units=output_dim => softmax
    model.add(Flatten())
    model.add(Dense(units=output_dim,
                    activation='softmax'))  # Cette couche permet de faire en sorte que la sortie soit un vecteur de somme 1 qu'on assimile à des probas pour chaque label

    model.compile(loss='categorical_crossentropy',
                  optimizer=RMSprop(lr=lr),
                  metrics=['accuracy'])

    return model


# Création du réseau de neurones avec transfert learning
def build_cnn_transfer_learning(input_dim, output_dim, d_fc, lr=0.00005):
    print('Importation des paramètres...')
    print('Build model...')

    # Importation des poids du modèle pré-entrainé
    model = build_cnn_eeg(input_dim, 10, 0.8)
    model.load_weights("cnn_egg_10classes.h5")

    # On freeze les couches déjà entrainées
    for layer in model.layers:
        layer.trainable = False

    # On ajoute les couches pour réduire le nombre de classes
    # Couche 2 : Dense = fullconnection, units = Nombre de classes
    model.add(Flatten())
    model.add(Dense(units=200, activation='relu'))
    # Couche 3 : Dropout (désactive des sorties de neurones aléatoirement avec une probabilité prédéfinie)
    model.add(Dropout(rate=0.5))
    model.add(Dense(units=200, activation='relu'))
    # Couche 4 : Dense, units=output_dim => softmax
    model.add(Dense(units=output_dim,
                    activation='softmax'))  # Cette couche permet de faire en sorte que la sortie soit un vecteur de somme 1 qu'on assimile à des probas pour chaque label

    model.compile(loss='categorical_crossentropy',
                  optimizer=RMSprop(lr=lr),
                  metrics=['accuracy'])

    return model


# Entrainement du modèle principal : sur 3 ou 10 classes
def train(temoin, class_weight, epoch, validstep, spe, dfc, length_valid, batch_size, outdim=3):
    # Paramètrage initial
    cnn_model = build_cnn_eeg((temoin[0], temoin[1], 1), outdim, dfc)
    cnn_model.summary()

    if outdim == 3:
        dataset = "data"
        savename = 'poids/cnn_egg_dfc' + str(dfc) + '_Epch' + str(epoch) + '_SPE' + str(spe) + '_VS' + str(validstep) + '.h5'
    if outdim == 10:
        dataset = "data10"
        savename = 'poids/cnn_egg_10classes.h5'

    # Importation des données
    train_generator = ImageDataGenerator()
    train_flow = train_generator.flow_from_directory(directory=dataset+"/train",
                                                     target_size=temoin,
                                                     color_mode="grayscale",
                                                     class_mode='categorical')

    valid_generator = ImageDataGenerator()
    valid_flow = valid_generator.flow_from_directory(directory=dataset+"/valid",
                                                     color_mode="grayscale",
                                                     target_size=temoin,
                                                     class_mode='categorical')

    # tensorboard --logdir=./logs
    tb_call_back = TensorBoard(log_dir='./logs',
                               histogram_freq=0,
                               write_graph=True,
                               write_images=True)

    cb = [tb_call_back]

    if outdim == 3:
        metrics_k = MetricsKappa()
        cb.append(metrics_k)

    # Lancement du modèle
    cnn_model.fit_generator(train_flow,
                            epochs=epoch,
                            steps_per_epoch=spe,
                            validation_steps=validstep,
                            validation_data=valid_flow,
                            callbacks=cb)

    cnn_model.evaluate_generator(valid_flow, steps=length_valid // batch_size)
    cnn_model.save_weights(savename)

    return savename


def train_transfert_learning(temoin, class_weight, epoch, validstep, spe, dfc, length_valid, batch_size, outdim=3):
    # Paramètrage initial
    cnn_model = build_cnn_transfer_learning((temoin[0], temoin[1], 1), outdim, dfc)
    cnn_model.summary()

    dataset = 'data10_3'

    # Importation des données
    train_generator = ImageDataGenerator()
    train_flow = train_generator.flow_from_directory(directory=dataset+"/train",
                                                     target_size=temoin,
                                                     color_mode="grayscale",
                                                     class_mode='categorical')

    valid_generator = ImageDataGenerator()
    valid_flow = valid_generator.flow_from_directory(directory=dataset+"/valid",
                                                     color_mode="grayscale",
                                                     target_size=temoin,
                                                     class_mode='categorical')

    # tensorboard --logdir=./logs
    tb_call_back = TensorBoard(log_dir='./logs',
                               histogram_freq=0,
                               write_graph=True,
                               write_images=True)

    metrics_k = MetricsKappa()

    # Lancement du modèle
    cnn_model.fit_generator(train_flow,
                            epochs=epoch,
                            steps_per_epoch=spe,
                            validation_steps=validstep,
                            validation_data=valid_flow,
                            class_weight=class_weight,
                            callbacks=[tb_call_back, metrics_k])

    cnn_model.evaluate_generator(valid_flow, steps=length_valid // batch_size)
    savename = 'poids/cnn_egg_tl_dfc' + str(dfc) + '_Epch' + str(epoch) + '_SPE' + str(spe) + '_VS' + str(validstep) + '.h5'
    cnn_model.save_weights(savename)
    return savename
