#!/usr/bin/python
# -*- coding: latin-1 -*-

from sklearn.metrics import cohen_kappa_score, confusion_matrix
import keras
import os
from keras.callbacks import TensorBoard, LambdaCallback, ModelCheckpoint
from keras.models import Sequential
from keras.layers import Dense, Activation, Conv2D, MaxPooling2D, Flatten, Dropout, Input, LSTM
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.optimizers import RMSprop
import numpy as np
import scipy
import random
import matplotlib.pyplot as plt
import pandas as pd
import pickle as pickle


# 6 : gun-shot
# 7 : jackhammer
# 8 : siren

# 1 colonne = 1 fenêtre : les valeurs des fréquences pour la fenêtre
# 1 ligne = 1 fréquence donnée


# Une solution trouvée fonctionnant d'après un papier pour des électroencéphalogramme
# Importation des données en STFT
# https://pos.sissa.it/299/001/pdf
# Ce réseau peut être utilisé sur les 10 ou les 3 classes
def build_cnn_eeg(input_dim, output_dim, d_fc, lr=0.00005):
    print('Importation des paramètres...')
    print('Build model...')
    model = Sequential()
    # Couche 1 : CNN + Maxpool
    model.add(keras.layers.Conv2D(15, (3, 3), input_shape=input_dim, activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    # Couche 2 : CNN + Maxpool
    model.add(keras.layers.Conv2D(40, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    # Couche 3 : CNN + Maxpool
    model.add(keras.layers.Conv2D(400, (3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 1)))
    # Couche 4 : Dense = fullconnection, units = Nombre de classes
    model.add(Dense(units=200, activation='linear'))
    # Couche 6 : Dropout (désactive des sorties de neurones aléatoirement avec une probabilité prédéfinie)
    model.add(Dropout(rate=d_fc))
    # Couche 5 : Dense, units=output_dim => softmax
    model.add(Flatten())
    model.add(Dense(units=output_dim,
                    activation='softmax'))  # Cette couche permet de faire en sorte que la sortie soit un vecteur de somme 1 qu'on assimile à des probas pour chaque label

    model.compile(loss='categorical_crossentropy',
                  optimizer=RMSprop(lr=lr),
                  metrics=['accuracy'])

    return model


# Fonction effectuant le preprocessing pour chaque image. Elle renvoie un tableau dans la bonne dimension.
# Le tableau renvoyé peut directement être lu par le NN et une prédiction peut être lancée.
def preprocessing(path, sysout=False):
    if sysout:
        print("Convertion tableau " + str(path) + " en image.")
    scipy.misc.imsave(path.replace("npy", "png"), np.load(path))
    ndimage = scipy.misc.imread(path.replace("npy", "png"))
    ndimage = np.expand_dims(ndimage, axis=0)
    ndimage = np.expand_dims(ndimage, axis=3)
    return ndimage


def predict_model(filelist, mode="default", truth_tab=None, path_poids="poids/cnn_egg_dfc0.9_Epch28_SPE15.0_VS16.0.h5"):
    # ATTENTION: cette fonction est là pour vous donner un exemple de fonction predict. Vous pouvez/devez modifier
    # le fonctionnement de celle-ci pour votre propre cas. N'oubliez pas d'y inclure l'éventuel prétraitement que vous
    # faites sur vos données avant de le passer au réseau de neurones.
    # ===>TESTEZ LA VOUS MEME AVANT<===

    model = build_cnn_eeg((129, 186, 1), 3, 0.8)
    model.load_weights(path_poids)
    model.compile(loss='categorical_crossentropy',
                  optimizer=RMSprop(lr=0.00005),
                  metrics=['accuracy'])

    predictions_tab = []

    for filename in filelist:
        ndimage = preprocessing(filename)
        prediction = model.predict(ndimage)
        predictions_tab.append(np.argmax(prediction))

    if mode == "default":
        return predictions_tab

    elif mode == "confmat":
        kappa = cohen_kappa_score(truth_tab, predictions_tab, labels=[0, 1, 2])
        conf_mat = confusion_matrix(truth_tab, predictions_tab, labels=[0, 1, 2])
        return kappa, conf_mat

